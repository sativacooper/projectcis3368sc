package cis.projectsc;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import org.springframework.stereotype.Component;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class MainController implements Initializable {

    //--------------------------------------------------Filling Table Data----------------------------------------------
    @FXML TableView<Employee> emp_table;
    @FXML TableColumn<Employee, String> emp_fname;
    @FXML TableColumn<Employee, String> emp_lname;
    @FXML TableColumn<Employee, String> emp_num;
    ObservableList<Employee> list = FXCollections.observableArrayList();

    @FXML TableView<Job> job_table;
    @FXML TableColumn<Job, String> ord_num;
    @FXML TableColumn<Job, String> shirts;
    @FXML TableColumn<Job, String> shirtd;
    @FXML TableColumn<Job, String> shirtc;
    @FXML TableColumn<Job, String> ord_by;
    ObservableList<Job> Jlist = FXCollections.observableArrayList();
    @FXML TextField enumfield;
    @FXML TextField elnamefield;
    @FXML TextField efnamefield;

    db base;

    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        System.setProperty("java.awt.headless", "false");
        base = new db();

//View Employee Data and add to table under Employee tab----------------------------------------------------------------
        try {
            Connection con = base.getconnection();

            ResultSet rs = con.createStatement().executeQuery("SELECT * FROM employee");

            while (rs.next()) {
                list.add(new Employee(rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        emp_fname.setCellValueFactory(new PropertyValueFactory<Employee, String>("firstName"));
        emp_lname.setCellValueFactory(new PropertyValueFactory<Employee, String>("lastName"));
        emp_num.setCellValueFactory(new PropertyValueFactory<Employee, String>("emNum"));
        emp_table.setItems(list);
        emp_table.setEditable(true);

 //View Job Data and add to table under Job tab-------------------------------------------------------------------------
        try {
            Connection con = base.getconnection();
            ResultSet js = con.createStatement().executeQuery("SELECT * FROM job");

            while (js.next()) {
                Jlist.add(new Job(js.getString(2), js.getString(3), js.getString(4), js.getString(5), js.getString(6)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ord_num.setCellValueFactory(new PropertyValueFactory<Job, String>("poNum"));
        shirts.setCellValueFactory(new PropertyValueFactory<Job, String>("shirtSize"));
        shirtc.setCellValueFactory(new PropertyValueFactory<Job, String>("shirtColor"));
        shirtd.setCellValueFactory(new PropertyValueFactory<Job, String>("shirtDesign"));
        ord_by.setCellValueFactory(new PropertyValueFactory<Job, String>("designLocation"));

        job_table.setItems(Jlist);


//Adding Items from DummyInfo into table--------------------------------------------------------------------------------
        newlist.getItems().addAll(buildInitialData());
        newlist.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


    }

// --------------------------------Filling Dashboard Table Data with Dummy Information----------------------------------


    private ObservableList<DummyInfo> buildInitialData() {

        ObservableList<DummyInfo> list = FXCollections.observableArrayList();
        newlist.getItems().addAll(
        new DummyInfo("8717078962", "Valeria Giannassi"),
        new DummyInfo("5543963483", "Ingram Dolling"),
        new DummyInfo("1778923127", "Sarena Hesbrook"),
        new DummyInfo("8439883811", "Abbot Cranny"),
        new DummyInfo("0302557261", "Gregoire Rosindill"),
        new DummyInfo("8812924883", "Selig Bothram"),
        new DummyInfo("3424146994", "Elna Cyphus"),
        new DummyInfo("8071535044", "Selig Bothram"),
        new DummyInfo("1342718011", "Oralie Rubroe"),
        new DummyInfo("9622620698", "Leesa Asty"),
        new DummyInfo("4629019871", "Clementine Brabban"),
        new DummyInfo("3917467658", "Vergil Huntington"),
        new DummyInfo("0010349960", "Henka Mussen")
                );

        return list;
    }

    private void removeSelectedItems() {
        List<DummyInfo> selectedjobs = new ArrayList<>();
        for (DummyInfo dummyInfo : newlist.getSelectionModel().getSelectedItems()) {
            selectedjobs.add(dummyInfo);
        }
        newlist.getSelectionModel().clearSelection();

        newlist.getItems().removeAll(selectedjobs);
    }


//---------------------------------------------------------------Employee Management------------------------------------
    @FXML
    Button addnewemp;




//Add New Employee to Database and table--------------------------------------------------------------------------------
    @FXML
    private void addnewempaction() throws SQLException {

        String m3 = enumfield.getText();
        String m1 = efnamefield.getText();
        String m2 = elnamefield.getText();

        PreparedStatement stmts = null;
        try {
            Connection con = base.getconnection();
            stmts = con.prepareStatement("INSERT INTO employee( first_name,last_name,em_num) VALUES (?, ?,?)");

            stmts.setString(1, m1);
            stmts.setString(2, m2);
            stmts.setString(3, m3);


            stmts.execute();
            stmts.close();

            JOptionPane.showMessageDialog(null, "Employee Added Successfully");
            enumfield.clear();
            elnamefield.clear();
            efnamefield.clear();


        } catch (SQLException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }

        list.add(new Employee(m1, m2, m3));
    }


    @FXML
    private void delempbutton() {

        PreparedStatement stmts = null;
        String selected = emp_num.getText();

        try {
            Connection con = base.getconnection();
            stmts = con.prepareStatement("DELETE FROM employee Where em_num= ?");

            stmts.setString(1, selected);


            stmts.execute();
            stmts.close();

            JOptionPane.showMessageDialog(null, "Employee Deleted Successfully");
            Employee selectedItem = emp_table.getSelectionModel().getSelectedItem();
            emp_table.getItems().remove(selectedItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

        // --------------------------------------------------------------------------------------------------------------------
    @FXML
    private TextField job2;

    @FXML
    private TextField job5;

    @FXML
    private TextField job4;

    @FXML
    private TextField job3;

    @FXML
    private TextField job1;


    @FXML
    private void addjob() throws SQLException {

        String j1 = job1.getText();
        String j2 = job2.getText();
        String j3 = job3.getText();
        String j4 = job4.getText();
        String j5 = job5.getText();

        PreparedStatement jstmts = null;
        try {
            Connection con = base.getconnection();
            jstmts = con.prepareStatement("INSERT INTO job( po_num,shirt_size, shirt_color, shirt_design,design_location) VALUES (?, ?,?,?,?)");

           jstmts.setString(1, j1);
            jstmts.setString(2, j2);
            jstmts.setString(3, j3);
            jstmts.setString(4, j4);
            jstmts.setString(5, j5);



            jstmts.execute();
            jstmts.close();

            JOptionPane.showMessageDialog(null, "Job Added Successfully");
            job4.clear();
            job5.clear();
            job3.clear();
            job2.clear();
            job1.clear();


        } catch (SQLException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Jlist.add(new Job(j1, j2, j3, j4, j5));
    }


    @FXML
    private void deljobbutton() {

        PreparedStatement stmts = null;
        String selected = job1.getText();

        try {
            Connection con = base.getconnection();
            stmts = con.prepareStatement("DELETE FROM job Where po_num= ?");

            stmts.setString(1, selected);


            stmts.execute();
            stmts.close();

            JOptionPane.showMessageDialog(null, "Job Deleted Successfully");
            Job selectedItem = job_table.getSelectionModel().getSelectedItem();
            job_table.getItems().remove(selectedItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }





//MouseEvent/Listeners for dragging and dropping between tables of New, In Porduction, and Close Out--------------------

    private static final DataFormat Job_List = new DataFormat("projectsc/jobList");
    @FXML
    private ListView<DummyInfo> newlist;

    @FXML
    private ListView<DummyInfo> prolist;

    @FXML
    private ListView<DummyInfo> clolist;



    // Dragging from New to In Production
    public void dragNewDetected(MouseEvent mouseEvent) {
        int selected = newlist.getSelectionModel().getSelectedIndices().size();
        if (selected > 0) {
            Dragboard dragboard = newlist.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<DummyInfo> selectedItems = new ArrayList<>(newlist.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
           content.put(Job_List, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            mouseEvent.consume();
        }

    }

    // Draggin over to In Production
    public void dragOverpro(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(Job_List)) {
            dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);

        }
        dragEvent.consume();
    }



    //Finish Dragging to In Production
    public void dragDroppedOnpro(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(Job_List)) {
            ArrayList<DummyInfo> dummyInfos = (ArrayList<DummyInfo>) dragboard.getContent(Job_List);
            dummyInfos.forEach(job -> job.setStatus(DummyInfo.Status.InProduction));
            prolist.getItems().addAll(dummyInfos);
            dragCompleted = true;
        }
        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }


    //Finish Drag Event from New
    public void dragDoneOnnew(DragEvent dragEvent) {
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            removeSelectedItems();
        }
        dragEvent.consume();
    }

    // Dragging from In Production to Close Out
    public void dragproDetected(MouseEvent mouseEvent) {
        int selected = prolist.getSelectionModel().getSelectedIndices().size();
        if (selected > 0) {
            Dragboard dragboard = prolist.startDragAndDrop(TransferMode.COPY_OR_MOVE);
            ArrayList<DummyInfo> selectedItems = new ArrayList<>(prolist.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(Job_List, selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            mouseEvent.consume();
        }

    }

    //Dragging finished from In Production to Close Out
    public void dragOverclose(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(Job_List)) {
            dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);

        }
        dragEvent.consume();
    }

    //Dropping Dragged information to Close Out from In Production
    public void dragDroppedOnclose(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(Job_List)) {
            ArrayList<DummyInfo> dummyInfos = (ArrayList<DummyInfo>) dragboard.getContent(Job_List);
            dummyInfos.forEach(job -> job.setStatus(DummyInfo.Status.CloseOut));
            clolist.getItems().addAll(dummyInfos);
            dragCompleted = true;
        }
        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    //Dragging finshed from In Production to Close Out
    public void dragDoneOnpro(DragEvent dragEvent) {
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if (tm == TransferMode.MOVE) {
            removeSelectedItems();
        }
        dragEvent.consume();
    }
}




