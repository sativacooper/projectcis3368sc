package cis.projectsc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import javafx.scene.image.Image;

@SpringBootApplication
public class ProjectscApplication extends javafx.application.Application {

    @Autowired
    private ConfigurableApplicationContext springContext;
    private Parent root;

    public static void main(String[] args) {
        launch();
    }


    @Override
    public void init() throws Exception {
        springContext = SpringApplication.run(ProjectscApplication.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main.fxml"));

        fxmlLoader.setControllerFactory(springContext::getBean);
        root = fxmlLoader.load();
        super.init();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
    primaryStage.setScene(new Scene(root));
        primaryStage.getIcons().add(new Image("book.png"));
        primaryStage.setTitle("Jay's T-Shirt Management System");
        primaryStage.show();

    }
}
