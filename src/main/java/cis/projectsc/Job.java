package cis.projectsc;

import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;
import java.io.*;
import java.sql.Date;
import java.util.Objects;
import java.io.Serializable;

@Entity
@Table(name = "job", schema = "public", catalog = "cisproject")
public class Job implements Serializable {
    private int id;
    private final SimpleStringProperty poNum;
    private final SimpleStringProperty shirtSize;
    private final SimpleStringProperty shirtColor;
    private final SimpleStringProperty shirtDesign;
    private final SimpleStringProperty designLocation;
    private Date orderDate;
    private Date pickupDate;
    private Status status;



    public enum Status {
        New, InProduction, CloseOut
    }

    public Job(String poNum, String shirtSize, String shirtColor, String shirtDesign,String designLocation) {
        this.poNum = new SimpleStringProperty(poNum);
        this.shirtSize = new SimpleStringProperty(shirtSize);
        this.shirtColor = new SimpleStringProperty(shirtColor);
        this.shirtDesign = new SimpleStringProperty(shirtDesign);
        this.designLocation = new SimpleStringProperty(designLocation);
        this.status = Status.New;

    }

    public void setStatus(Status status){
        this.status = status;
    }

    public Status getStatus(){
        return status;
    }

    public String getPoNum() {
        return poNum.get();
    }

    public SimpleStringProperty poNumProperty() {
        return poNum;
    }

    public void setPoNum(String poNum) {
        this.poNum.set(poNum);
    }

    public String getShirtSize() {
        return shirtSize.get();
    }

    public SimpleStringProperty shirtSizeProperty() {
        return shirtSize;
    }

    public void setShirtSize(String shirtSize) {
        this.shirtSize.set(shirtSize);
    }

    public String getShirtColor() {
        return shirtColor.get();
    }

    public SimpleStringProperty shirtColorProperty() {
        return shirtColor;
    }

    public void setShirtColor(String shirtColor) {
        this.shirtColor.set(shirtColor);
    }

    public String getShirtDesign() {
        return shirtDesign.get();
    }

    public SimpleStringProperty shirtDesignProperty() {
        return shirtDesign;
    }

    public void setShirtDesign(String shirtDesign) {
        this.shirtDesign.set(shirtDesign);
    }

    public String getDesignLocation() {
        return designLocation.get();
    }

    public SimpleStringProperty designLocationProperty() {
        return designLocation;
    }

    public void setDesignLocation(String designLocation) {
        this.designLocation.set(designLocation);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Basic
    @Column(name = "order_date", nullable = true)
    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Basic
    @Column(name = "pickup_date", nullable = true)
    public Date getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(Date pickupDate) {
        this.pickupDate = pickupDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Job job = (Job) o;
        return id == job.id &&
                Objects.equals(poNum, job.poNum) &&
                Objects.equals(shirtSize, job.shirtSize) &&
                Objects.equals(shirtColor, job.shirtColor) &&
                Objects.equals(shirtDesign, job.shirtDesign) &&
                Objects.equals(designLocation, job.designLocation) &&
                Objects.equals(orderDate, job.orderDate) &&
                Objects.equals(pickupDate, job.pickupDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, poNum, shirtSize, shirtColor, shirtDesign, designLocation, orderDate, pickupDate);
    }


    @Override
    public String toString() {

        return String.format("%s - %s - (%s)",poNum.get(),designLocation.get(), this.status);
    }


    private void readObjectNoData() throws ObjectStreamException
    {
        throw new InvalidObjectException("Stream data required");
    }
}



