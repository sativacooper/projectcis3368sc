package cis.projectsc;

import java.io.Serializable;

public class DummyInfo implements Serializable {

        private String PO;
        private String Name;


        // This is the list of possible values for the status of the person
        public enum Status {
            New, InProduction, CloseOut
        }

        private Status status;


        public DummyInfo(String PO, String Name) {
            this.PO = PO;
            this.Name = Name;
            this.status = Status.New; // default status
        }

        public void setStatus(Status status){
            this.status = status;
        }

        public Status getStatus(){
            return status;
        }


        @Override
        public String toString() {

            return String.format("%s - %s - (%s)",this.PO, this.Name,this.status);
        }

        public String getPO() {
            return PO;
        }

        public void setPO(String ID) {
            this.PO = ID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String firstName) {
            this.Name = firstName;
        }
    }

