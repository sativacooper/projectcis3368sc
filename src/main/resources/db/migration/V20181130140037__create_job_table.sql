create table job
(
id SERIAL PRIMARY KEY NOT NULL,
po_num varchar(24),
shirt_size varchar(24),
shirt_color varchar(24),
shirt_design varchar(24),
design_location varchar(24),
order_date date,
pickup_date date
)